#pragma once

#include "Global.h"

namespace stbx {    
    bool moveFile(const path &oldp, const path &newp);
    bool makeDir(const path &p);
    bool rmFile(const path &p);
    bool rmDir(const path &p);
    boost::int64_t fileRead(ifstream &file, char *read_buffer, int chars_to_read);
    boost::int64_t getFileSize(ifstream &file);
}


