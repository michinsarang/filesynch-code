#pragma once

// standard libs
#include <iostream>
#include <string>
#include <map>
#include <sys/stat.h>
#include <sys/types.h> // ino_t
#include <stdio.h>
#include <sstream>
#include <time.h>
//boost
#include <boost/format.hpp>
#include <boost/cstdint.hpp>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <boost/any.hpp>
// SQlite
#include <sqlite3.h>

using namespace std;
using namespace boost::filesystem;
using namespace boost::program_options;
using boost::format;
using boost::io::group;

static format updQuery("UPDATE %1% SET %2% = %3% WHERE %4% = %5%;");
static format selQuery("SELECT %1% FROM %2% %3%;");
static format insQuery("INSERT INTO %1% (%2%) VALUES (%3%);");

const int bUFSIZ = 999;

#define ARRAY_SIZE(x) (sizeof(x)/sizeof(x[0]))
struct db_col_t {
    const string col_name;
    const string col_type;
};

struct db_table_t {
    db_table_t(string tn, const size_t n, const db_col_t col[]) 
    : table_name(tn), num_columns(n), columns(col) { }
    const string table_name;
    const size_t num_columns;
    const db_col_t* columns;
};

struct db_t {
    //db_t(string fn, const size_t n, const db_table_t t[]) 
    //: file_name(fn), num_tables(n), tables(t) { }
    string file_name;
    size_t num_tables; 
    const db_table_t* tables;
};

///
const string FILE_TABLE_NAME = "filedb";
const db_col_t FILE_COL_NAMES[] = {
    {"id", "INTEGER PRIMARY KEY"},
    {"filename", "TEXT"},
    {"filenamehash", "INTEGER"},
    {"modified", "DATE"},
    {"hash", "INTEGER"},
    {"size", "INTEGER"},
    {"inode", "INTEGER"},
    {"gid", "INTEGER"},
    {"uid", "INTEGER"},
    {"dirname_id", "INTEGER"},
    {"dirty", "INTEGER"}
};
const db_table_t FILE_TABLE(
    FILE_TABLE_NAME,
    ARRAY_SIZE(FILE_COL_NAMES),
    FILE_COL_NAMES
);
///
const string DIR_TABLE_NAME = "dirname";
const db_col_t DIR_COL_NAMES[] = {
    {"id", "INTEGER PRIMARY KEY"},
    {"filepath", "TEXT"},
    {"filepathhash", "INTEGER"}
};
const db_table_t DIR_TABLE(
    DIR_TABLE_NAME,
    ARRAY_SIZE(DIR_COL_NAMES),
    DIR_COL_NAMES
);
///
const string EXCLUSION_TABLE_NAME = "exclusion";
const db_col_t EXCLUSION_COL_NAMES[] = {
    {"id", "INTEGER PRIMARY KEY"},
    {"filename", "TEXT"}
};
const db_table_t EXCLUSION_TABLE(
    EXCLUSION_TABLE_NAME,
    ARRAY_SIZE(EXCLUSION_COL_NAMES),
    EXCLUSION_COL_NAMES
);
///
const string QUEUED_ACTION_TABLE_NAME = "queuedaction";
const db_col_t QUEUED_ACTION_COL_NAMES[] = {
    {"id", "INTEGER PRIMARY KEY"},
    {"modified", "INTEGER"},
    {"action", "INTEGER"},
    {"src", "TEXT"},
    {"dest", "TEXT"}
};
const db_table_t QUEUED_ACTION_TABLE(
    QUEUED_ACTION_TABLE_NAME,
    ARRAY_SIZE(QUEUED_ACTION_COL_NAMES),
    QUEUED_ACTION_COL_NAMES
);
///
const db_table_t TABLES[] = {
    FILE_TABLE,
    QUEUED_ACTION_TABLE,
    DIR_TABLE,
    EXCLUSION_TABLE
};

static format configFormat("%1%=%2%");
static string CONFIGFILENAME = "database.ini";

struct config_table {
    const string key;
    const string default_value;
    const string description;
};

const string DBFILELOC = "dbFileLocation";
const string DBFILENAME = "dbFileName";
const string PKEYPATH = "pkeyPath";
const string USERNAME = "username";

const config_table CONFIGTABLE[] = {

    { DBFILELOC, "./", "file path of database"},
    { DBFILENAME, "files.db", "name of database"},
    { PKEYPATH, "./", "file path of the private key"},
    { USERNAME, "username", "username"},
};