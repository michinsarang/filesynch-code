#pragma once

#include "ConfigDb.h"
#include "MakeHash.h"
#include <fstream>

using namespace std;
using namespace boost::filesystem;
using boost::format;
using boost::io::group;

enum FileTableEntries {
    FILENAME,
    FILENAME_HASH,
    MODIFIED,
    FILEHASH,
    SIZE,
    INODE,
    GID,
    UID
};

enum CurrentInstruction {
    // File Instructions
    FILE_REMOVE,
    FILE_RENAMED,
    FILE_NEW,
    FILE_MOVE_RENAMED,
    FILE_MOVED,
    FILE_COPY,
    FILE_NONE,

    // Update Instructions
    UPDATE,
    NO_UPDATE
};

class FileInfo {
private:

public:
    FileInfo(const path &p, bool gen_full_hash);

    FileInfo(unsigned int ph,
        unsigned int fh,
        string pn, 
        string fn,
        long mt, 
        ino_t ino, 
        gid_t gid, 
        uid_t uid, 
        size_t sz,
        int64_t flh = 0);

    // Alt constructor
    FileInfo(const FileInfo &other);
    
    ~FileInfo();

    string getFileSql(int dir_key);
    string getDirSql(int dir_key);

    uint32_t path_name_hash;
    uint32_t file_name_hash;
    int64_t full_path_hash;
    string path_name;
    string file_name;
    string full_path_name;
    time_t mod_time; 
    ino_t inode;
    gid_t gid;
    uid_t uid;
    size_t size;
    int nlink;
    int64_t file_hash;

    bool valid;
    bool processed;

    enum CurrentInstruction instruction;
    enum CurrentInstruction update_instruction;

    string mvrn_to_fname;
    string mvrn_to_dir;
    uint32_t mvrn_to_fname_hash;
    uint32_t mvrn_to_dir_hash;
    int64_t to_file_hash;
    long to_mod_time;
    size_t to_size;
};

typedef map<ino_t, FileInfo*> FileInodeMap;
typedef map<string, FileInfo*> FilenameMultimap;

struct FileDbMaps {
    FilenameMultimap* file_db_map_by_filename;
    FileInodeMap* file_db_map_by_inode;
};

const string FILE_INFO_SQL = "SELECT dirname.filepathhash, filedb.filenamehash, dirname.filepath, filedb.filename, filedb.modified, "
                             "filedb.inode, filedb.gid, filedb.uid, filedb.size, filedb.hash "
                             "FROM filedb INNER JOIN dirname ON dirname.id=filedb.dirname_id;";


class Database {

private:
    sqlite3 *m_db;

    string m_DbFilename;
    db_t m_DatabaseTable;
   
    FileInodeMap m_PastFsMapByInode;
    FilenameMultimap m_PastFsMapByFname;

    FileInodeMap m_CurrentFsMapByInode;
    FilenameMultimap m_CurrentFsMapByFname;

    vector<FileInfo*> m_NodeInstructions;
    vector<path> m_Directories;

    bool m_HashFiles;
    bool m_ShowUpdates;

    string getPopulateDatabaseSql(vector<path> v);

    void buildDb(vector<path> v);
    bool tableExists(const string &table, const string database_file);
    string getDbCreateSql();
    void createDbFile();

    string insertValuesFullFileInfo(const path &file_name, const unsigned int &file_name_hash, const time_t &last_write_time, const int64_t &full_file_hash, const size_t &file_size,
                          const ino_t &st_ino, const gid_t &st_gid, const uid_t &st_uid, const int &path_key);
    string updateSpecificFileDbVals(FileInfo *file_info, vector<FileTableEntries> col_vals, bool to_values);

    void insertNewFileInfo(FileInfo *file_info);
    int retrieveDirectoryKey(const path &dir, bool insert_key);
    string checkValueExistQuery(const string &table_name, const string &col_type, const string &value, bool numeric_value = false, string optional = "1");
    string directoryQuery(const path &dir);
    string deleteFileQuery(const path &file_name, const int &path_key);
    string deleteDirQuery(const int &path_key);
    void removeFileDirectory(FileInfo *file_info);

    void syncDb(vector<path> v);
    void getPastFsState();
    void getCurrentFsState(vector<path> v);
    void fsSync();

    void findCopyNewFiles();
    void findDelUpdateFiles();
    void findInodeMatchFiles();

    void clearFsMaps();

    void updateDbSyncProcessor();

    void constructConfigFile(string config_file_name);
    void parseConfigFile(string config_file_name, options_description& desc, variables_map& config_map);

public:
    Database(const path& watch_dir, bool build_flag, bool hash_flag = true, bool show_updates = true);

    void openDb(const string &path);
    void closeDb();

    void updateQuery(string& query, 
            const string& upd_col_name, const string& upd_value, 
            const string& col, const string& value, int tableindex);
    void selectQuery(string& query, const string& values, int tableindex, const string& where = " ");
    int execQuery(const string &sql, int (*cb)(void*, int, char**, char**), void *arg);

    string getDbFilename() { return m_DbFilename; }
};