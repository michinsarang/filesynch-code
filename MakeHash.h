#pragma once

// Standard libs
#include <iostream>
#include <fstream>
#include <stdexcept>
// Boost
#include <boost/cstdint.hpp>
#include <boost/interprocess/sync/file_lock.hpp>
#include <boost/filesystem.hpp>
#include <boost/interprocess/sync/sharable_lock.hpp>
#include <boost/thread/thread.hpp>
#include <boost/bind.hpp>

#include "xxhash.h"
#include "LocalFileSysOps.h"

using namespace std;
using namespace boost::filesystem;
using namespace boost::interprocess;

const int BLOCK_LENGTH = 262144*sizeof(char); // hash block length for incremental hash: 256 mb
const unsigned int SEED = 5;

class MakeHash {

    private:
        int64_t m_FileHash;
        unsigned int m_HashOne;
        unsigned int m_HashTwo;
        void makeHash(ifstream &file, int64_t length, unsigned int &hash);
        XXH_errorcode incrementHash(char *file_block, ifstream &file, int64_t &total_length_read, void *state, int64_t &length);
    public:
        int makeFullHash(string p);
        int makeSparseHash(string p);
        int64_t getHash() { return m_FileHash; }
        void setClearHash() { m_FileHash = 0; }
};
