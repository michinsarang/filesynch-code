#include "MakeHash.h"

/* makes a 64 bit hash from a file passed by string p and stores it in m_FileHash */
int MakeHash::makeFullHash(string p) {

    setClearHash();
    int64_t half_length_one = 0;
    int64_t half_length_two = 0;
    int64_t length = 0;
    
    ifstream file_first_half(p.c_str(), ios::in|ios::binary);
    ifstream file_second_half(p.c_str(), ios::in|ios::binary);
    
    try {
        file_lock f_lock(p.c_str());
        sharable_lock<file_lock> sh_lock(f_lock);
    } catch (boost::interprocess::interprocess_exception &ec) {
        cerr << "[Error] attempting to lock file : " << p << " " << ec.what() << endl;
    }


    // reads in file and gets lengths
    if (file_first_half && file_second_half && file_first_half.is_open() && file_second_half.is_open()) {
        length = stbx::getFileSize(file_first_half);
        half_length_one = length/2;                             // length to read for hash 1
        half_length_two = length - half_length_one;             // length to read for hash 2
        file_second_half.seekg(half_length_one+1, file_first_half.beg);
    } else {
        cerr << "[Error] Unable to open file" << endl;
        return -1;
    }
    
    
    
    // threading function calls to make each 32 bit hash
    boost::thread thread_one(boost::bind(&MakeHash::makeHash, this, boost::ref(file_first_half), half_length_one, boost::ref(this->m_HashOne)));
    boost::thread thread_two(boost::bind(&MakeHash::makeHash, this, boost::ref(file_second_half), half_length_two, boost::ref(this->m_HashTwo)));
    thread_one.join();
    thread_two.join();
    
    // Merge the two 32 bit hashes into a 64 bit hash
    m_FileHash = (int64_t)(((int64_t)m_HashOne) << 32 | ((int64_t)m_HashTwo));

    return 0;   //TODO error handling
}

/* makes a 32 bit hash */
void MakeHash::makeHash(ifstream &file, int64_t length, unsigned int &hash) {
    int64_t total_length_read = 0;
    void *state = XXH32_init(SEED);
    char file_block[BLOCK_LENGTH];
    
    if (file.is_open()) {
        while(file && (total_length_read < length)) {
            if (MakeHash::incrementHash(file_block, file, total_length_read, state, length) == XXH_ERROR) {
                // TODO we may need to do something strange to 
                // throw a proper error in a thread. Just inform for now.
                cerr << "[Error] incrementHash failed." << endl;
            }
            
        }
    } else {
        cerr << "[Error] file is not open." << endl;
    }
    //delete[] file_block;

    if ((total_length_read != length) && (total_length_read != length-1)) {
        cerr << "[Error] file not completely read." << endl;
    }

    file.close();
    hash = XXH32_digest(state);
    
    return;
}

/* increments the current incremental hash with new input */
XXH_errorcode MakeHash::incrementHash(char *file_block, ifstream &file, int64_t &total_length_read, void *state, int64_t &length) {

    int64_t length_read = 0;
    XXH_errorcode err_code;
    int64_t smaller_read = length - total_length_read;

    if (!file) {
        cerr << "[Error] invalid file" << endl;
    }
    
    // check if the file_first_half is near the half file length point
    if ((smaller_read) < BLOCK_LENGTH) {
        length_read = stbx::fileRead(file, file_block, smaller_read);
    } else {
        length_read = stbx::fileRead(file, file_block, BLOCK_LENGTH);
    }

    err_code = XXH32_update(state, file_block, length_read);

    total_length_read += length_read; 
    memset(file_block, 0, BLOCK_LENGTH);
    return err_code;
}