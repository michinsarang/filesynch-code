#include "LocalFileSysOps.h"

namespace stbx {

    static void printEc(error_code &ec) {
        cerr << "[Error] " << ec.message() << endl;
    }

    /* function to move local files, also renames */
    bool moveFile(const path &oldp, const path &newp) {
        error_code ec;
        if (exists(oldp, ec)) {
            rename(oldp, newp, ec);
        } else {
            printEc(ec);
            return false;
        }
        return true;
    }

    /* function to create directory */
    bool makeDir(const path &p) {
        error_code ec;
        if (!is_directory(p, ec)) {
            if (!create_directory(p, ec)) {
                printEc(ec);
                return false;
            }
        } else {
            printEc(ec);
            return false;
        }
        return true;
    }

    /* function to delete files */
    bool rmFile(const path &p) {
        error_code ec;
        if (exists(p, ec)) {
            if (!remove(p, ec)) {
                printEc(ec);
                return false;
            }
        } else {
            printEc(ec);
            return false;
        }
        return true;
    }

    /* function to recursively remove files in directory */
    bool rmDir(const path &p) {
        error_code ec;
        if (exists(p, ec)) {
            if (!remove_all(p, ec)) {
                printEc(ec);
                return false;
            }
        } else {
            printEc(ec);
            return false;
        }
        return true;
    }

    /* read chars_to_read number of chars from file stream, returns number of characters read */
    boost::int64_t fileRead(ifstream &file, char *read_buffer, int chars_to_read) {
        int64_t read_count = 0;
        try {
            file.read(read_buffer, chars_to_read);
        }
        catch (ios_base::failure &ex) {
            cerr << "[ERROR] " << ex.what() << endl;
        }
        read_count = (int64_t)file.gcount();
        return read_count;
    }

    /* takes a file stream and returns file length */
    boost::int64_t getFileSize(ifstream &file) {
        int64_t file_size = 0;
        try {
            file.seekg(0, file.end);
            file_size = (int64_t)file.tellg();
            file.seekg(0, file.beg);
        }
        catch (ios_base::failure &ex) {
            cerr << "[ERROR] " << ex.what() << endl;
        }
        return file_size;
    }
}
