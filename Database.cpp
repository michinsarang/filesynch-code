#include "Database.h"

////// FileInfo Definitions //////

FileInfo::FileInfo(const path &p, bool gen_full_hash) {
    // Get stat data
    struct stat buf;
    valid = false;

    path_name = p.branch_path().generic_string();
    file_name = p.leaf().generic_string();
    full_path_name = p.generic_string();

    if(stat(full_path_name.c_str(), &buf) == -1) {
        cerr << "[Error] Could not stat file " << full_path_name
                << " it will not be tracked!" << endl;
        return;
    }
    inode = buf.st_ino;
    gid = buf.st_gid;
    uid = buf.st_uid;
    
    mod_time = last_write_time(p);
    size = file_size(p);

    // Generate hashes for file path and full path
    // Note: hash does not include '\0'
    path_name_hash = (uint32_t)XXH32((const void*)(path_name.c_str()), path_name.length(), SEED);
    file_name_hash = (uint32_t)XXH32((const void*)(file_name.c_str()), file_name.length(), SEED);
    full_path_hash = (int64_t)(((int64_t)path_name_hash) << 32 | ((int64_t)file_name_hash));

    if (gen_full_hash) {
        MakeHash mh;
        if (mh.makeFullHash(full_path_name)) {
            cerr << "[Error] Could not generate hash for file " << full_path_name << endl;
        } else {
            file_hash = mh.getHash();
        }
    } else {
        file_hash = 0;
    }

    processed = false;
        
    valid = true;

    // Sensible defaults
    update_instruction = NO_UPDATE;
    instruction = FILE_NONE;

}

FileInfo::~FileInfo() {
    // Move along. Nothing to see here.
}

FileInfo::FileInfo(uint32_t ph,
        uint32_t fh,
        string pn, 
        string fn,
        long mt, 
        ino_t ino, 
        gid_t gid, 
        uid_t uid, 
        size_t sz,
        int64_t flh)
        : path_name_hash(ph)
        , file_name_hash(fh)
        , full_path_hash((int64_t)(((int64_t)path_name_hash) << 32 | ((int64_t)file_name_hash)))
        , path_name(pn)
        , file_name(fn)
        , full_path_name(pn + "/" + fn)
        , mod_time(mt)
        , inode(ino)
        , gid(gid)
        , uid(uid)
        , size(sz)
        , file_hash(flh)
        , valid(true)
        , processed(false) {
    // Sensible defaults
    update_instruction = NO_UPDATE;
    instruction = FILE_NONE;
}

string FileInfo::getFileSql(int dir_key) {
    // iron out FILE_COL_NAMES so this can be turned into a more general loop structure
    ostringstream sql_ss;
    sql_ss << "INSERT INTO " << FILE_TABLE_NAME << " (" 
        << FILE_COL_NAMES[1].col_name << "," 
        << FILE_COL_NAMES[2].col_name << "," 
        << FILE_COL_NAMES[3].col_name << ","
        << FILE_COL_NAMES[4].col_name << ","
        << FILE_COL_NAMES[5].col_name << ","
        << FILE_COL_NAMES[6].col_name << ","
        << FILE_COL_NAMES[7].col_name << ","
        << FILE_COL_NAMES[8].col_name << ","
        << FILE_COL_NAMES[9].col_name << ")"
        << "VALUES (\""
        << file_name << "\","
        << file_name_hash << ","
        << mod_time << ","
        << file_hash << ","
        << size << ","
        << inode << ","
        << gid << ","
        << uid << ","
        << dir_key << ");" << endl;
    return sql_ss.str();
}

string FileInfo::getDirSql(int dir_key) {
    ostringstream sql_ss;
    sql_ss << "INSERT INTO " << DIR_TABLE_NAME << " (" 
            << DIR_COL_NAMES[0].col_name << "," 
            << DIR_COL_NAMES[1].col_name << ","
            << DIR_COL_NAMES[2].col_name << ") VALUES ("
            << dir_key << ",\"" << path_name << "\"," << path_name_hash << ");";
    return sql_ss.str();
}

////// End FileInfo //////

////// Primitive db functions //////

// Constructor
Database::Database(const path& watch_dir, bool build_flag, bool hash_flag, bool show_updates) {
    m_HashFiles = hash_flag;
    m_ShowUpdates = show_updates;

    options_description desc("options");
    variables_map config_map;

    constructConfigFile(CONFIGFILENAME);
    parseConfigFile(CONFIGFILENAME, desc, config_map);

    m_DbFilename =  boost::any_cast<string>(config_map[DBFILELOC].value()) + boost::any_cast<string>(config_map[DBFILENAME].value());

    m_DatabaseTable.file_name = m_DbFilename; // ugly hack
    m_DatabaseTable.num_tables = ARRAY_SIZE(TABLES);
    m_DatabaseTable.tables = TABLES;

    // Are we building the db for the first time?
    if (build_flag) {
        vector<path> pvec;
        pvec.push_back(watch_dir);
        copy(recursive_directory_iterator(watch_dir), 
                recursive_directory_iterator(), back_inserter(pvec));

        buildDb(pvec);
    }

}

/** Open the sqlite db at db_path */
void Database::openDb(const string &db_path) {
    if (sqlite3_open(db_path.c_str(), &m_db) != SQLITE_OK) {
        cerr << "[Error] Open database file " << db_path 
                << " failed with message: " << sqlite3_errmsg(m_db) << endl;
        // Not much we can do about this
        exit(1);
    }
}

/** Close the db */
void Database::closeDb() {
    sqlite3_close(m_db);
}

/** Executes a query using the given callback to return data in arg
 *
 *  Returns: 0 on success, 1 on failure
 */
int Database::execQuery(const string& sql, int (*cb)(void*, int, char**, char**), void *arg) {
    char *zErrMsg = NULL;
    // execute query for value insertion
    if (sqlite3_exec(m_db, "BEGIN TRANSACTION", NULL, NULL, &zErrMsg) != SQLITE_OK) {
        cerr << "[Error] Transaction failed with message: " << sqlite3_errmsg(m_db) << endl;
        goto bail;
    }
    
    if (sqlite3_exec(m_db, sql.c_str(), cb, arg, &zErrMsg) != SQLITE_OK) {
        cerr << "[Error] Query: " << sql << " failed with message: " 
                << sqlite3_errmsg(m_db) << endl;
        goto bail;
    }

    if (sqlite3_exec(m_db, "COMMIT TRANSACTION", NULL, NULL, &zErrMsg) != SQLITE_OK) {
        cerr << "[Error] Commit failed with message: " << sqlite3_errmsg(m_db) << endl;
        goto bail;
    }

    return 0;

bail:
    free(zErrMsg);
    return 1;
}


/** Takes a pointer to a sqlite database and creates the tables in the
 *  database if they don't already exist.                                  
 */
void Database::createDbFile() {
    if (execQuery(getDbCreateSql(), NULL, NULL) != 0) {
        // If we fail here we really can't go anywhere. Abort.
        cerr << "[Error] Could not create database file, aborting" << endl;
        exit(1);
    } 
}

/** Generate the sql required to initially populate the database 
 *  v is a vector of boost::path with all file paths of files to track
 */
string Database::getPopulateDatabaseSql(vector<path> v) {
    ostringstream sql_ss;
    int dir_idx = 0;
    map<path, int> seen_dirs;
    
    for (vector<path>::const_iterator it(v.begin()); it != v.end(); ++it) {
        if (is_regular_file(*it)) {
            FileInfo fi(*it, m_HashFiles);
            // if we see a new directory
            if (0 == seen_dirs.count(it->branch_path())) {
                ++dir_idx;
                seen_dirs[it->branch_path()] = dir_idx;
                sql_ss << fi.getDirSql(dir_idx);
            }
            sql_ss << fi.getFileSql(seen_dirs[it->branch_path()]);
        }
    }

    return sql_ss.str();
}


/** Takes a vector of paths and checks if a current database file  
 *  already exists. If it does not exist, create it and populate it
 *  with the vector of paths. If it does exist, sync the current fs state to it.       
 */
void Database::buildDb(vector<path> v) {
    // checks for existence of the database and makes sure it has the tables
    if (exists(m_DbFilename)) {
        // If so, sync past state to present
        if (tableExists(FILE_TABLE_NAME, m_DbFilename)) {
            syncDb(v);
            return;
        } else {
            cerr << "[Error] " << m_DbFilename << " is not a valid database file. Rebuilding..." << endl;
        }
    }

    openDb(m_DbFilename);

    createDbFile();

    if (execQuery(getPopulateDatabaseSql(v), NULL, NULL) != 0) {
        cerr << "[Error] Database could not be populated, aborting." << endl;
        exit(1);
    } else {
        cout << "[Log] Done creating database." << endl;
    } 

    closeDb();
}

/* Checks for table existence */
bool Database::tableExists(const string &table, const string database_file) {
    int rc;
    bool table_exists = false;

    if (sqlite3_open_v2(database_file.c_str(), &m_db, SQLITE_OPEN_READONLY, NULL) == SQLITE_OK) {
        sqlite3_stmt *stmt;
        ostringstream sql_ss;
        sql_ss << "pragma table_info(" << table << ");";

        if (sqlite3_prepare_v2(m_db, sql_ss.str().c_str(), -1, &stmt, NULL) != SQLITE_OK) {
            cerr << "[Error] sqlite3_prepare_v2 returned: " << sqlite3_errmsg(m_db) << endl;
        }

        if ((rc = sqlite3_step(stmt)) != SQLITE_DONE && rc != SQLITE_ROW) {
            cerr << "[Error] sqlite3_step returned: " << sqlite3_errmsg(m_db) << endl;
        }

        // If we get a positive count, we're good
        if (sqlite3_data_count(stmt) > 0) {
            table_exists = true;
        }

        sqlite3_finalize(stmt);
        sqlite3_close(m_db);

    } else {
        cerr << "[Error] sqlite3_open returned: " << sqlite3_errmsg(m_db) << endl;
    }

    return table_exists;
}

/** Get the sql string to create the new empty database */
string Database::getDbCreateSql() {
    string create("CREATE TABLE IF NOT EXISTS ");
    ostringstream query;
    for (size_t i = 0; i < m_DatabaseTable.num_tables; ++i) {
        query << create << m_DatabaseTable.tables[i].table_name << " (";
        for (size_t j = 0; j < m_DatabaseTable.tables[i].num_columns; ++j) {
            if (j) {
                query << ", ";
            }
            query << m_DatabaseTable.tables[i].columns[j].col_name 
                    << " " << m_DatabaseTable.tables[i].columns[j].col_type;
        }
        query << ");\n";
    }
    string sql(query.str());
    return sql;
}

static int existCb(void* param, int num_cols, char** res, char** header) {
    int* exist = (int*)param;
    (*exist) = (num_cols > 0) ? atoi(res[0]) : -1;
    return 0;
}

/** Find directory key, if it exists return the path key number
 *  if it does not exist, and insert_key is true, 
 *  create the directory in the database, return the path key number.
 *
 *  Note: Requires an open database.
 */
int Database::retrieveDirectoryKey(const path &file_name, bool insert_key) {
    int exist, rc;

    ostringstream err_ss;
    err_ss << "[Error] Could not execute retrieveDirectoryKey on directory " 
        << file_name.generic_string() << " : ";

    string sql = checkValueExistQuery(DIR_TABLE_NAME, DIR_COL_NAMES[1].col_name, file_name.generic_string(), false, FILE_COL_NAMES[0].col_name);
    if ((rc = execQuery(sql, &existCb, (void*)&exist)) != SQLITE_OK) {
        cerr << err_ss.str() << sqlite3_errmsg(m_db) << endl;
    }

    // If the file_name is not in the db,
    // insert value then retrieve the path key number
    if (exist == -1) {
        if (insert_key) {
            ostringstream sql_ss;
            sql_ss << directoryQuery(file_name) 
                    << checkValueExistQuery(DIR_TABLE_NAME, FILE_COL_NAMES[1].col_name
                    , file_name.generic_string(), false, FILE_COL_NAMES[0].col_name);
            
            if (execQuery(sql_ss.str(), &existCb, (void*)&exist) != 0) {
                cerr << err_ss.str() << sqlite3_errmsg(m_db) << endl;
            }
        } else {
            cerr << "[Error] Could not find directory key, insert_key == false for file " 
                    << file_name.generic_string() << endl;
        }
    }

    return exist;
}

/** Optional to retrieve specific value, 
 *  checks only for path existence in the db.
 */
string Database::checkValueExistQuery(const string &table_name, const string &col_type, const string &value, bool numeric_value, string optional)  {
    ostringstream sql_ss;
    sql_ss << "SELECT " << optional << " FROM " << table_name << " WHERE " << col_type << "=" ;
    if (numeric_value == false) {
        sql_ss << "'";
    }
    sql_ss << value;
    if (numeric_value == false) {
        sql_ss << "'";
    }
    sql_ss << ";" << endl;
    
    return sql_ss.str();
}

string Database::directoryQuery(const path &dir) {
    ostringstream sql_ss;
    sql_ss << "INSERT INTO " << DIR_TABLE_NAME << " (" << DIR_COL_NAMES[1].col_name << ") VALUES (" << dir << ");" << endl;
    return sql_ss.str();
}

string Database::deleteFileQuery(const path &file_name, const int &path_key) {
    ostringstream sql_ss;
    sql_ss << "DELETE FROM " << FILE_TABLE_NAME << " WHERE " << FILE_COL_NAMES[9].col_name << "=" 
        << path_key << " AND " << FILE_COL_NAMES[1].col_name << "=" << file_name << ";" << endl;
    return sql_ss.str();
}

string Database::deleteDirQuery(const int &path_key) {
    ostringstream sql_ss;
    sql_ss << "DELETE FROM " << DIR_TABLE_NAME << " WHERE " << FILE_COL_NAMES[0].col_name << "=" << path_key << endl;
    return sql_ss.str();
}

/* Removes a file and provides cleanup for directories, requires an open database */
void Database::removeFileDirectory(FileInfo *file_info) {
    int dir_key, rc;

    ostringstream err_ss;

    // retrieve the directory key
    dir_key = retrieveDirectoryKey(path(file_info->path_name), false);

    // delete file by filename and directory key
    string sql = deleteFileQuery(path(file_info->file_name), dir_key);
    if ((rc = execQuery(sql, &existCb, (void*)&dir_key)) != 0) {
        cerr << "[Error] Could not execute retrieveDirectoryKey and deleteFile on file " 
                << file_info->full_path_name << sqlite3_errmsg(m_db) << endl;
    }

    // check if any files exist within the directory keyed by dir_key
    sql = checkValueExistQuery(FILE_TABLE_NAME, FILE_COL_NAMES[9].col_name, boost::lexical_cast<string>(dir_key), true);
    err_ss.clear();
    err_ss << "[Error] Could not execute checkValueExist on file " 
        << file_info->full_path_name << " : ";

    if (execQuery(sql, &existCb, (void*)&dir_key) != 0) {
        cerr << err_ss.str() << sqlite3_errmsg(m_db) << endl;
    }

    if (dir_key == -1) {
        path file_path;
        // delete the directory if it doesn't currently exist on the current filesystem
        if(path(file_info->path_name) == ".") {
            file_path = "./";
        } else {
            file_path = path(file_info->path_name);
        }

        if (find(m_Directories.begin(), m_Directories.end(), file_path) == m_Directories.end()) {
            sql = deleteDirQuery(dir_key);
            if (execQuery(sql, &existCb, (void*)&dir_key) != 0) {
                cerr << err_ss.str() << sqlite3_errmsg(m_db) << endl;
            }
        }
    }
}

/* constructs an update SQL query string */
void Database::updateQuery(string& query, 
        const string& upd_col_name, const string& upd_value, 
        const string& col, const string& value, int tableindex) {
    string queryadd = boost::str(updQuery
            % (m_DatabaseTable.tables[tableindex].table_name)
            % upd_col_name
            % upd_value
            % col
            % value);
    query.append(queryadd);
}

/** construct a retrieve SQL query string 
 *  query is the current query string, value is the column value to retrieve,
 *  static format retquery("SELECT %1% from %2%;");
 */
void Database::selectQuery(string& query, const string& values, int tableindex, const string& where) {
    string queryadd = boost::str(selQuery
            % values
            % (m_DatabaseTable.tables[tableindex].table_name)
            % where); // Optional WHERE clause.
    query.append(queryadd);
}

inline static long s2l(const char* str) {
    return strtol(str, NULL, 10);
}
inline static unsigned int s2i(const char* str) {
    return strtoul(str, NULL, 10);
}

static int populate_fileinfo_map_cb(void* param, int num_cols, char** res, char** header) {
    for (int i = 0; i < num_cols; ++i) {
        if (res[i] == NULL) {
            cerr << "[Error] Query returned NULL in populate_fileinfo_map_cb" << endl;
            return 1;
        }
    }
    FileDbMaps *m = (FileDbMaps*)param;
    
    FileInfo* file_info = new FileInfo(
          s2i(res[0])    // path hash
        , s2i(res[1])    // filename hash
        , string(res[2]) // path
        , string(res[3]) // file name
        , s2l(res[4])    // mod time
        , (ino_t)s2l(res[5])    // inode
        , s2l(res[6])    // gid
        , s2l(res[7])    // uid
        , (size_t)s2l(res[8])       // size (bytes)
        , boost::lexical_cast<int64_t>(res[9])); // file hash

    // Sensible defaults
    file_info->update_instruction = NO_UPDATE;
    file_info->instruction = FILE_NONE;
    file_info->valid = true;

    m->file_db_map_by_inode->insert(pair<ino_t, FileInfo*>(file_info->inode, file_info));
    m->file_db_map_by_filename->insert(pair<string, FileInfo*>(file_info->file_name, file_info));

    return 0;
}

void Database::getPastFsState() {
    // open file database and retrieve past state
    openDb(m_DbFilename);
    FileDbMaps m;
    m.file_db_map_by_filename = &m_PastFsMapByFname;
    m.file_db_map_by_inode = &m_PastFsMapByInode;
    if(execQuery(FILE_INFO_SQL, &populate_fileinfo_map_cb, (void*)&m) != 0) {
        cerr << "[Error] Could not get past fs state." << endl;
    } 
    closeDb();
}

void Database::getCurrentFsState(vector<path> v) {
    for (vector<path>::iterator it(v.begin());  it != v.end(); ++it) {
        if (is_regular_file(*it)) {
            FileInfo* fi = new FileInfo(*it, m_HashFiles);
            m_CurrentFsMapByInode[fi->inode] = fi;
            m_CurrentFsMapByFname[fi->file_name] = fi;
        } else if (is_directory(*it)) {
            m_Directories.push_back(*it);
        }
    }
}

void Database::syncDb(vector<path> v) {
    getPastFsState();
    getCurrentFsState(v);
    fsSync();

    //updateDbSyncProcessor();
    clearFsMaps();
}

void Database::fsSync() {
    findInodeMatchFiles();
    findDelUpdateFiles();
    findCopyNewFiles();
}

////// Helpers for clearing maps //////
inline static void notNullFreeFi(FileInfo* &fi) {
    if (fi != NULL) {
        delete fi;
        fi = NULL;
    }
}

/** Frees memory held by all FileInfo pointers 
 */
void Database::clearFsMaps() {
    for (FilenameMultimap::iterator it(m_PastFsMapByFname.begin()); it != m_PastFsMapByFname.end(); ++it) {
        notNullFreeFi(it->second);
    }
    for (FilenameMultimap::iterator it(m_CurrentFsMapByFname.begin()); it != m_CurrentFsMapByFname.end(); ++it) {
        notNullFreeFi(it->second);
    }
}

/** Helper to check two FileInfo objects to see 
 *  if the data of one differs from that of the other.
 *  If so, populates prev_fi with information relevant to
 *  other hosts for update.
 */
inline static void checkForUpdate(FileInfo* &prev_fi, FileInfo* &cur_fi, bool hash_flag, bool show_updates) {
    if ((prev_fi->size != cur_fi->size && prev_fi->mod_time != cur_fi->mod_time) 
            || (hash_flag && prev_fi->file_hash != cur_fi->file_hash)) {

        if (show_updates) {
            cout << prev_fi->full_path_name << " has been updated." << endl;
        }

        prev_fi->update_instruction = UPDATE;
        prev_fi->to_file_hash = cur_fi->file_hash;
        prev_fi->to_mod_time = cur_fi->mod_time;
        prev_fi->to_size = cur_fi->size;
    } else {
        prev_fi->update_instruction = NO_UPDATE;
    }
}

/** Iterates through the previous filesystem searching 
 *  for matching inodes in current filesystem.
 * 
 *  Files that have been MOVED, RENAMED, or BOTH are marked as such.
 *  All files that aren't marked as processed (i.e. they haven't been moved or renamed) 
 *  are inserted into the path map for the next stage (findDelUpdateFiles).
 */
void Database::findInodeMatchFiles() {

    // For every inode in the previous filesystem state
    for (FileInodeMap::iterator prev_fs_it(m_PastFsMapByInode.begin())
            ; prev_fs_it != m_PastFsMapByInode.end()
            ; ++prev_fs_it) {

        FileInfo* prev_fi = prev_fs_it->second;
        // if the FileInfo doesn't exist or isn't valid, skip it because nothing will work.
        if (prev_fi == NULL || prev_fi->valid == false) {
            cerr << "[Warning] Properties of " << prev_fi->file_name << " cannot be determined." << endl;
            continue;
        }

        // Search map for file with matching inode, O(log(n))
        FileInodeMap::iterator cur_fs_it(m_CurrentFsMapByInode.find(prev_fi->inode)); 

        FileInfo* cur_fi = NULL;
        // if match found assign to cur_fi
        if (cur_fs_it != m_CurrentFsMapByInode.end() 
                && cur_fs_it->second != NULL 
                && cur_fs_it->second->valid) {

            cur_fi = cur_fs_it->second;
        } else {
            // No inode match, assume not renamed or moved, skip.
            continue;
        }

        // Beyond this point cur_fi and prev_fi have the same inode

        if (prev_fi->full_path_name != cur_fi->full_path_name) { 
            // Inodes match but paths don't. The file has been moved, renamed, or both.

            if (prev_fi->file_name != cur_fi->file_name && prev_fi->path_name != cur_fi->path_name) {
                // The file has been both moved to a different directory and renamed.
                if (m_ShowUpdates) {
                    cout << prev_fi->full_path_name << " has been moved and renamed." << endl;
                }
                prev_fi->instruction = FILE_MOVE_RENAMED;
                cur_fi->instruction = FILE_MOVE_RENAMED;
            } else if (prev_fi->file_name != cur_fi->file_name) { 
                // Only file names differ.
                if (m_ShowUpdates) {
                    cout << prev_fi->full_path_name << " has been renamed." << endl;
                }
                prev_fi->instruction = FILE_RENAMED;
                cur_fi->instruction = FILE_RENAMED;
            } else if (prev_fi->path_name != cur_fi->path_name) {
                // Only path names differ.
                if (m_ShowUpdates) {
                    cout << prev_fi->full_path_name << " has been moved." << endl;
                }
                prev_fi->instruction = FILE_MOVED;
                cur_fi->instruction = FILE_MOVED;
            }

            // Based on the determined action, populate metadata for update
            if (prev_fi->instruction == FILE_MOVE_RENAMED || prev_fi->instruction == FILE_RENAMED) {
                prev_fi->mvrn_to_fname = cur_fi->file_name;
                prev_fi->mvrn_to_fname_hash = cur_fi->file_name_hash;

            } 
            if (prev_fi->instruction == FILE_MOVE_RENAMED || prev_fi->instruction == FILE_MOVED) {
                prev_fi->mvrn_to_dir = cur_fi->path_name;
                prev_fi->mvrn_to_dir_hash = cur_fi->path_name_hash;
            }

            // Check to see if we have to handle a file data update
            checkForUpdate(prev_fi, cur_fi, m_HashFiles, m_ShowUpdates);

            prev_fi->processed = true;
            // Copy pointer into the instruction book
            m_NodeInstructions.push_back(prev_fi);
        }
    }
}

/** All files that aren't marked as updated or deleted from 
 *  the previous stage (findInodeMatchFiles) are considered. 
 *
 *  Iterates through previous filesystem and compares them with items in the 
 *  current filesystem, finding file matches by file path hash to determine whether 
 *  the file has been updated or deleted from the current filesystem.
 */
void Database::findDelUpdateFiles() {
    // For every filename in the previous filesystem state
    for (FilenameMultimap::iterator prev_fs_it(m_PastFsMapByFname.begin())
            ; prev_fs_it != m_PastFsMapByFname.end()
            ; ++prev_fs_it) {
        
        FileInfo* prev_fi = prev_fs_it->second;
        // We may already know what happened to this file from the previous stage. If we do, skip.
        if (prev_fi->processed == true) {
            continue;
        }

        // if the FileInfo doesn't exist or isn't valid, skip it, nothing will work.
        // This indicates a stat error earlier.
        if (prev_fi == NULL || prev_fi->valid == false) {
            cerr << "[Warning] Properties of " << prev_fi->file_name 
                << " cannot be determined, (possible stat error)." << endl;
            continue;
        }

        // Search multimap for file(s) with matching filename, O(klg(n))
        pair<FilenameMultimap::iterator, FilenameMultimap::iterator> cur_fs_match_it;
        cur_fs_match_it = m_CurrentFsMapByFname.equal_range(prev_fi->file_name); 


        // Beyond this point prev_fi and any in cur_fs_match_it 
        // share file names and do not have the same inode


        if (cur_fs_match_it.first == cur_fs_match_it.second) {
            // Match not found. The file was deleted.
            if (m_ShowUpdates) {
                cout << prev_fi->full_path_name << " has been deleted" << endl;
            }

            prev_fi->instruction = FILE_REMOVE;
            prev_fi->update_instruction = NO_UPDATE;
        } else {
            for (FilenameMultimap::iterator it(cur_fs_match_it.first); it != cur_fs_match_it.second; ++it) {
                // Look to see if the directory names match too.
                if (it->second->path_name == prev_fi->path_name) {
                    // Same file! Check to see if we have to handle a file data update.
                    checkForUpdate(prev_fi, it->second, m_HashFiles, m_ShowUpdates);
                }
            }
        }

        prev_fi->processed = true;
        // Push into the instruction book
        m_NodeInstructions.push_back(prev_fi);
    }
}

/** All files that aren't marked as updated or deleted 
 *  from the previous function are considered. 
 *
 *  Iterates through the current filesystem searching for 
 *  file name hash matches between the current and previous 
 *  filesystem to determine whether a file is a copy of another. 
 *  Otherwise the file is considered new.
 */
void Database::findCopyNewFiles() {
    // For every filename in the current filesystem state
    for (FilenameMultimap::iterator cur_fs_it(m_CurrentFsMapByFname.begin())
            ; cur_fs_it != m_CurrentFsMapByFname.end()
            ; ++cur_fs_it) {
        
        FileInfo* cur_fi = cur_fs_it->second;
        // if the FileInfo doesn't exist or isn't valid, skip it, nothing will work.
        // This indicates a stat error earlier.
        if (cur_fi == NULL || cur_fi->valid == false) {
            cerr << "[Warning] Properties of " << cur_fi->file_name 
                    << " cannot be determined, (possible stat error)." << endl;
            continue;
        }

        // Search multimap for file(s) with matching filename, O(klg(n))
        pair<FilenameMultimap::iterator, FilenameMultimap::iterator> prev_fs_match_it;
        prev_fs_match_it = m_PastFsMapByFname.equal_range(cur_fi->file_name); 

        // Beyond this point cur_fi and any in prev_fs_match_it 
        // share file names, do not have the same inode, 
        // are in different directories

        if (prev_fs_match_it.first == prev_fs_match_it.second && cur_fi->instruction == FILE_NONE) {
            // Match not found. The file is new.
            if (m_ShowUpdates) {
                cout << cur_fi->full_path_name << " is new." << endl;
            }

            cur_fi->instruction = FILE_NEW;
            cur_fi->update_instruction = UPDATE;
            m_NodeInstructions.push_back(cur_fi);
        } else {
            // TODO Fix copy
            /*
            for (FilenameMultimap::iterator it(prev_fs_match_it.first); it != prev_fs_match_it.second; ++it) {
                if ((it->second->processed == false 
                        && it->second->path_name != cur_fi->path_name 
                        && it->second->size != cur_fi->size) 
                        || (m_HashFiles && it->second->file_hash == cur_fi->file_hash)) {
                    if (m_ShowUpdates) {
                        cout << cur_fi->full_path_name << " has been copied from " 
                            << it->second->full_path_name << endl;
                    }

                    m_NodeInstructions.push_back(it->second);

                }
            }
            */
        }
    }
}

void Database::updateDbSyncProcessor() {
    openDb(m_DbFilename);
    for (vector<FileInfo*>::iterator fiit = m_NodeInstructions.begin(); fiit != m_NodeInstructions.end(); ++fiit) {
        // check with other clients to see if fiit->file_name has the highest mod time
        // if (forall otherclient.find(fiit->file_name).mod_time < fiit->mod_time) {
        vector<FileTableEntries> col_vals;
        string sql;

        switch((*fiit)->instruction) {
            case FILE_REMOVE:
                // find in db, remove
                removeFileDirectory(*fiit);
                break;
            case FILE_RENAMED:
                // find in db, rename filename
                col_vals.push_back(FILENAME);
                col_vals.push_back(FILENAME_HASH);

                if ((*fiit)->update_instruction == UPDATE) {
                    // change hash, mod time, size
                    col_vals.push_back(SIZE);
                    col_vals.push_back(MODIFIED);
                    col_vals.push_back(FILEHASH);
                }

                sql = updateSpecificFileDbVals(*fiit, col_vals, true);
                if ((execQuery(sql, NULL, NULL)) != 0) {
                    cerr << "[Error] Could not insert file info for file update " << (*fiit)->mvrn_to_fname << " : " << sqlite3_errmsg(m_db) << endl;
                }

                break;
            case FILE_NEW:
                // add to db
                break;
            case FILE_MOVE_RENAMED:
                // find in db, change filename, filepath
                if ((*fiit)->update_instruction == UPDATE) {
                    // change hash, mod time, size
                }
                break;
            case FILE_MOVED:
                // find in db, change path
                if ((*fiit)->update_instruction == UPDATE) {
                    // change hash, mod time, size
                }
                break;
            case FILE_COPY:
                // add to db
                break;
            default:
                // No op
                break;
        }
        // local file action update push into outgoing queueprocessor
    }
    // } else {
    //      request file info from client with highest mod time, update database and sync.
    // }
    closeDb();
}

void Database::insertNewFileInfo(FileInfo *file_info) {
    openDb(m_DbFilename);
    
    int dir_key = retrieveDirectoryKey(path(file_info->path_name), true);

    if (execQuery(file_info->getFileSql(dir_key), NULL, NULL) != 0) {
        cerr << "[Error] Could not insert new file info for file " 
                << file_info->path_name << " : " << sqlite3_errmsg(m_db) << endl;
    }

    closeDb();
}

/** update a specific file by file_name with values in a vector of enumerators that coincide with value names
 *  to_values specifies whether it uses the to_(value) entries of the FileInfo struct.                        
 */
string Database::updateSpecificFileDbVals(FileInfo *file_info, vector<FileTableEntries> col_vals, bool to_values) {
    
    ostringstream sql_ss;
    ostringstream sqlVal;
    string val_name;
    sql_ss << "UPDATE " << FILE_TABLE.table_name;
    
    for (vector<FileTableEntries>::iterator it=col_vals.begin(); it != col_vals.end(); ++it) {
        switch(*it) {
            case FILENAME:
                val_name = "filename";
                if (to_values) {
                    sqlVal << file_info->mvrn_to_fname;
                } else {
                    sqlVal << file_info->file_name;
                }
                break;
            case FILENAME_HASH:
                val_name = "filenamehash";
                if (to_values) {
                    sqlVal << file_info->mvrn_to_fname_hash;
                } else {
                    sqlVal << file_info->file_name_hash;
                }
                break;
            case MODIFIED:
                val_name = "modified";
                if (to_values) {
                    sqlVal << file_info->to_mod_time;
                } else {
                    sqlVal << file_info->mod_time;
                }
                break;
            case FILEHASH:
                val_name = "hash";
                if (to_values) {
                    sqlVal << file_info->to_file_hash;
                } else {
                    sqlVal << file_info->file_hash;
                }
                break;
            case SIZE:
                val_name = "size";
                if (to_values) {
                    sqlVal << file_info->to_size;
                } else {
                    sqlVal << file_info->size;
                }
                break;
            case INODE:
                val_name = "inode";
                sqlVal << file_info->inode;
                break;
            case GID:
                val_name = "gid";
                sqlVal << file_info->gid;
                break;
            case UID:
                val_name = "uid";
                sqlVal << file_info->uid;
                break;
                //case DIRTY:
                //    val_name = "dirty";
                //    break;
        }
        sql_ss << " SET " << val_name << "=" << sqlVal.str();
        sqlVal.clear();
    }

    sql_ss << " WHERE "
        << file_info->file_name << "=" << FILE_COL_NAMES[1].col_name << ";"
        << endl;
    return sql_ss.str();
}

void Database::constructConfigFile(string config_file_name) { // add user supplied info later?
    if (!exists(config_file_name)) {
        ofstream config_stream(config_file_name.c_str());
        if (config_stream.is_open()) {
            for (unsigned int i=0; i < ARRAY_SIZE(CONFIGTABLE); i++ ) {
                config_stream << configFormat %CONFIGTABLE[i].key %CONFIGTABLE[i].default_value << endl;
            }
        }
        config_stream.close();
    }
}

/* values must contain the same number of strings as the number of config table options */
void Database::parseConfigFile(string config_file_name, options_description& desc, variables_map& config_map) {
    
    for (unsigned int i=0; i < ARRAY_SIZE(CONFIGTABLE); i++ ) {
        desc.add_options()
            (CONFIGTABLE[i].key.c_str(), value<string>(), CONFIGTABLE[i].description.c_str());
    }
    ifstream config_stream(config_file_name.c_str());

    store(parse_config_file(config_stream, desc), config_map);
    notify(config_map);
    config_stream.close();
}